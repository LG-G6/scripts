# LineageOS

##  Getting Started
To initialize your local repository, download tree/kernel and other stuff use this ninja command:

```bash
mkdir los18 && cd los18 && git clone https://github.com/LG-G6/scripts.git -b lineage-18.1 && repo init -u https://codeberg.org/LineageOS/android.git -b lineage-18.1 && export USE_CCACHE=1 && export CCACHE_EXEC=/usr/bin/ccache && ccache -M 50G && mkdir .repo/local_manifests && if [ -d lg-g6_manifests ];then rm -rf lg-g6_manifests;fi && git clone https://codeberg.org/LG-G6/local_manifests -b lineage-18.1 lg-g6_manifests && cp lg-g6_manifests/roomservice.xml .repo/local_manifests/ && . scripts/sync-g6.sh && make clean
```
